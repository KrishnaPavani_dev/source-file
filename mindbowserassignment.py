# To run this, download the BeautifulSoup zip file
# and unzip it in the same directory as this file

# import statements 
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE



url ='https://www.acehardware.com/departments/outdoor-living/grills-and-smokers/gas-grills'
html = urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, "html.parser")

item_id=[]
item_name=[]
item_image_link=[]
item_price=[]
item_description=[]

# Retrieve all of the anchor tags with class corresponding to links of items

tags = soup('a',attrs={'class' : 'mz-productlisting-title'})
for tag in tags:
     # finding each item url
     link = tag.get('href', None)
     # get into each item url
     linkurl = link
     linkhtml = urlopen(linkurl, context=ctx).read()
     linksoup = BeautifulSoup(linkhtml, "html.parser")
     # get into each item specifications
     item_id.append(linkurl.split('/')[-1])
     
     item_name.append('"'+linksoup.find('h1',{'class','mz-pagetitle'}).text+'"')
     
     item_price.append('"'+linksoup.find('span',{'class','custom-price'}).text+'"')
     
     item_image_link.append('https:'+linksoup.find('img',{'class','mz-productimages-mainimage'})['src'])
     
     item_description.append('"'+linksoup.find('div', {'id': 'mobileProductDetailsContainer'}).text+'"')
     
     print('fetching data...')
    
    

# headers in the csv
fields=["ID","Item Name","Price","Description","ImageURL"]
# defining csv filename
filename= "result.csv"
rows=list(zip(item_id,item_name,item_price,item_description,item_image_link))

#writing to csv file
with open(filename, 'w') as f:  
    f.write(','.join(fields)+'\n')
    for i in rows:
        f.write(','.join(i)+'\n')
        
              
         
         
     
                    
    
